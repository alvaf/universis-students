import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FullLayoutComponent} from '../layouts/full-layout.component';
import {AuthGuard} from '../auth/guards/auth.guard';
import {ProfileHomeComponent} from "./components/profile-home/profile-home.component";
import {ProfileDetailsComponent} from "./components/profile-details/profile-details.component";

const routes: Routes = [
    {
        path: '',
        component: ProfileHomeComponent,
        canActivate: [
            AuthGuard
        ]
    },
    {
        path: 'details',
        component: ProfileDetailsComponent,
        canActivate: [
            AuthGuard
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule {
}
