import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable()
export class ProfileService {
    constructor(private context: AngularDataContext) {

    }

    getStudent() {
        return this.context.model('students/me')
            .asQueryable()
            .expand('person', 'user','department','studyProgram','inscriptionMode')
            .getItem().then(student=> {
                if (typeof student === 'undefined') {
                    return Promise.reject('Student data cannot be found');
                }
                return this.context.model('LocalDepartments')
                    .asQueryable()
                    .where('id').equal(student.department.id)
                    .getItem().then(department=> {
                        if (typeof department === 'undefined') {
                            return Promise.reject('Student department cannot be found');
                        }
                        //todo: validate getItem() result
                        student.department = department;
                        return Promise.resolve(student);
                    });
            });
    }

}
